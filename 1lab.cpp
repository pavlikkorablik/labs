#include <iostream>
#include "1lab.h"

namespace lab {

    row* rrow(row*& beg, int a) {
        row* hey = nullptr;
        if (beg == nullptr) {
            beg = new row;
            beg->num = a;
            beg->next = nullptr;
            beg->mas = nullptr;
            hey = beg;
        }
        else {
            row* ptr1 = nullptr;
            int check = 1;
            for (row* ptr = beg; ptr != nullptr; ptr = ptr->next) {
                if (ptr->num == a) {
                    hey = ptr;
                    check = 0;
                    break;
                }
                else if (ptr->num > a) {
                    row* pop = new row;
                    if (!ptr1) {
                        pop->next = beg;
                        beg = pop;
                    }
                    else {
                        ptr1->next = pop;
                        pop->next = ptr;
                    }
                    pop->num = a;
                    pop->mas = nullptr;
                    hey = pop;
                    check = 0;
                    break;
                }
                ptr1 = ptr;
            }
            if (check) {
                row* pop = new row;
                ptr1->next = pop;
                pop->next = nullptr;
                pop->num = a;
                pop->mas = nullptr;
                hey = pop;
            }
        }
        return hey;
    }

    int ccol(row* beg, row*& hey, int b) {
        column* pil;
        if (hey->mas == nullptr) {
            hey->mas = new column;
            pil = hey->mas;
            pil->num = b;
            pil->next = nullptr;
        }
        else {
            column* ptr2 = nullptr;
            int check = 1;
            for (column* ptr = hey->mas; ptr != nullptr; ptr = ptr->next) {
                if (ptr->num == b) {
                    pil = ptr;
                    check = 0;
                    break;
                }
                else if (ptr->num > b) {
                    column* plp = new column;
                    if (!ptr2) {
                        plp->next = hey->mas;
                        hey->mas = plp;
                    }
                    else {
                        ptr2->next = plp;
                        plp->next = ptr;
                    }
                    plp->num = b;
                    pil = plp;
                    check = 0;
                    break;
                }
                ptr2 = ptr;
            }
            if (check) {
                column* plp = new column;
                ptr2->next = plp;
                plp->next = nullptr;
                plp->num = b;
                pil = plp;
            }
        }
        std::cout << "Enter the value of the element" << std::endl;
        if (getNum(pil->val) < 0) {
            erase(beg);
            return 0;
        }
        return 1;
    }

    int input(row* &beg, int &n) {
        const char* pr = "";
        int a;
        int b;
        do {
            std::cout << pr << std::endl;
            std::cout << "Enter the dimension of the matrix" << std::endl;
            pr = "Incorrect data! Try again.";
            if (getNum(n) < 0) {
                erase(beg);
                return 0;
            }
        } while (n < 1);
        int check = 1;
        while (true) {
            pr = "";
            do {
                std::cout << pr << std::endl;
                std::cout << "If you want to stop adding elements, enter \"-100\"" << std::endl;
                std::cout << "Enter the number of the row" << std::endl;
                pr = "Incorrect data! Try again.";
                if (getNum(a) < 0) { 
                    erase(beg);
                    return 0;
                }
                if (a == -100) {
                    check = 0;
                    break; 
                }
            } while (a < 0 || a >= n);
            if (!check) break;
            pr = "";
            do {
                std::cout << pr << std::endl;
                std::cout << "Enter the number of the column" << std::endl;
                pr = "Incorrect data! Try again.";
                if (getNum(b) < 0) {
                    erase(beg);
                    return 0;
                }
            } while (b < 0 || b >= n);    
            row* forcol = rrow(beg, a);
            ccol(beg, forcol, b);        
        }
        return 1;
    }

    void output(row* beg, double* vec, int n) {
        std::cout << "\n\nSparsed matrix:" << std::endl;
        for (row* ptr = beg; ptr != nullptr; ptr = ptr->next) {
            for (column* ptr1 = ptr->mas; ptr1 != nullptr; ptr1 = ptr1->next) {
                std::cout << "(" << ptr->num << ")" << "(" << ptr1->num << ")" << ptr1->val << std::endl;
            }
        }
        std::cout << "\n\n";
        std::cout << "Vector: ";
        for (int i = 0; i < n; i++) {
            std::cout << "[" << vec[i] << "]";
        }
        std::cout << "\n\n";
    }

    int maxelem(column* ptr) {
        int k = 0;
        int maxx;
        if (ptr != nullptr) maxx = ptr->val;
        else return k;
        for (column* pp = ptr->next; pp != nullptr; pp = pp->next) {
            if (pp->val > maxx) {
                maxx = pp->val;
                k = pp->num;
            }
        }
        return k;
    }

    double find (int i, int j, row* beg) {
        for (row* ptr = beg; ptr != nullptr; ptr = ptr->next) {
            if (ptr->num == i) {
                for (column* ptr1 = ptr->mas; ptr1 != nullptr; ptr1 = ptr1->next) {
                    if (ptr1->num == j) return ptr1->val;
                    else if (ptr1->num > j) return 0;
                }
                return 0;
            }
            else if (ptr->num > i) return 0;
        }
        return 0;
    }

    int makevec(row* beg, double*& vec, int n) {
        try {
            vec = new double[n];
        }
        catch (std::bad_alloc &ba) {
            std::cout << "Problems with alloc: " << ba.what() << std::endl;
            return 0; 
        }
        int a = 0;
        for (row* ptr = beg; ptr != nullptr; ptr = ptr->next) {
            vec[ptr->num] = 0;
            for (int i = a; i < ptr->num; i++) vec[i] = 0;
            a = ptr->num + 1;
            int k = maxelem(ptr->mas);
            for (int j = 0; j < n; j++) {
                double m = find(ptr->num, j, beg);
                double n = find(j, k, beg);
                vec[ptr->num] += m*n;
            }
        }
        for (int l = a; l < n; l++) vec[l] = 0;
        return 1; 
    }

    void erase(row* beg) {
        row* ptr = beg;
        while (ptr != nullptr) {
            column* ptr1 = ptr->mas;
            while (ptr1 != nullptr) {
                column* ptr2 = ptr1;
                ptr1 = ptr1->next;
                delete ptr2;
            }
            row* ptr3 = ptr;
            ptr = ptr->next;
            delete ptr3;
        }
    }
}