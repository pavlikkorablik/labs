namespace lab {
    
    struct column {
        double val;
        column* next;
        int num;
    };
    
    struct row {
        int num;
        row* next;
        column* mas;
    };

    template <class B>
    int getNum(B &a) {
        std::cin >> a;
        if (!std::cin.good()) return -1;
        return 1;
    }

    int input(row*&, int&);
    row* rrow(row*&, int);
    int ccol(row*, row*&, int);
    void output(row*, double*, int);
    double find(int, int, row*);
    int makevec(row*, double*&, int);
    void erase(row*);
}