#include <iostream>
#include "1lab.h"

using namespace lab;

int main() {
    row* beg = nullptr;
    double* vec = nullptr;
    int n;
    input(beg, n);
    makevec(beg, vec, n) ;
    output(beg, vec, n);
    erase(beg);
    return 0;
}